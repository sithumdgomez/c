#include <stdio.h>
#include <math.h>

int main()
{
	int num,vel;
	const float mass=45.36;
	float k_energy;

	printf("\nEnter Velocity:");
	scanf("%d",&vel);
	printf("\n V | Kinetic Energy");

	if(vel % 2 == 0)
	{
		for(num=vel; num>0; num-=2)
		{
			k_energy = mass * pow(num,2);
			printf("\n%2d | %.2f",num,k_energy);

		}
		printf("\n\n");
	}
	else
	if(vel % 2 == 1)
	{
		vel-=1;
		for(num=vel; num>0; num-=2)
                {
                        k_energy = mass * pow(num,2);
                        printf("\n%2d | %.2f",num,k_energy);

                }
                printf("\n\n");
	}
return 0;
}
